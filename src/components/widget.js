import React, { Component, useEffect, useState, useRef } from "react";
import { Widget, addResponseMessage, addUserMessage } from "react-chat-widget";
import logo from "../logo.svg";
import "react-chat-widget/lib/styles.css";
import "./widget.css";
import axios from "axios";

function WidgetApp() {
  const nameRef = useRef();
  const emailRef = useRef();
  const messageRef = useRef();

  const [toggle, setToggle] = useState(true);
  const [flag, setFlag] = useState(false);
  const [nameDetails, setnameDetails] = useState(false);
  const [emailDetails, setemailDetails] = useState(false);

  const [msgDetails, setmsgDetails] = useState(false);

  const [conversation, setConversation] = useState([
    { message: "Hi", time: "12:15", sender: "user1", conversation_id: 123 },
    { message: "Welcome", time: "12:15", sender: "user", conversation_id: 123 },
    {
      message: "I have doubt",
      time: "12:15",
      sender: "user1",
      conversation_id: 123,
    },
    {
      message:
        "Yeah let me know the problem so that I  suggest solution for your problem",
      time: "12:15",
      sender: "user",
    },
    {
      message: "I am facing problem in creating account",
      time: "12:15",
      sender: "user1",
    },
    {
      message: "okay I will share a link so  that you can follow the steps",
      time: "12:15",
      sender: "user",
    },
  ]);
  // const set = conversation;
  useEffect(() => {
    addResponseMessage("Welcome, How may I help You?");
    var persons;
    const fetchData = async () => {
      await axios
        .get(`https://jsonplaceholder.typicode.com/users`)
        .then((res) => {
          persons = res;
          console.log(res.data);
          console.log("Hello");
          // debugger
          console.log([...conversation, persons.data[1], persons.data[2]]);
          

// debugger
console.log(conversation)

        });
    };
    fetchData();

    if (conversation.length !== 0) {
      conv();
      setToggle(true);
    }
  }, []);

  const conv = () => {
    console.log("inside");

    conversation.forEach((con) => {
      if (con.sender === "user") {
        addResponseMessage(con.message);
      } else {
        addUserMessage(con.message);
      }
    });
  };
  const handleNewUserMessage = async (newMessage) => {
    await axios
      .get(`https://jsonplaceholder.typicode.com/users`)
      .then((res) => {
        const persons = res;
        console.log(persons);

        // setConversation(...conversation,persons)
      });
    console.log(`New message incoming! ${newMessage}`);
    const messages = {
      message: newMessage,
      time: "5:44",
      sender: "user1",
      conversation_id: 123,
    };
    axios
      .post(`https://jsonplaceholder.typicode.com/users`, messages)
      .then((res) => {
        const persons = res;
        console.log(persons);
        // setConversation(...conversation,persons)
      });
    // addUserMessage(newMessage)
    // addResponseMessage(newMessage)
    // Now send the message throught the backend API
    // setConversation(conversation.push({"email":"arvi@gmail.com",message:newMessage,sender:"user"}))
    // setConversation(conversation.push(messages))

    console.log(messages ? messages : "not updated");
  };
  const clickHandler = () => {
    console.log("in toggle button");

    //  debugger
    if (flag === false && conversation.length === 0) {
      setFlag(true);
      setToggle(false);
    } else if (conversation.length !== 0) {
      setToggle(true);
    }
  };
  const getUserDetails = () => {
    const name = nameRef.current.value;
    const email = emailRef.current.value;
    const msg = messageRef.current.value;
    console.log(name);
    console.log(email);
    setToggle(false);
    // console.log(details)
    // debugger
    axios
      .post(`https://jsonplaceholder.typicode.com/users`, {
        name: nameRef.current.value,
        email: emailRef.current.value,
      })
      .then((res) => {
        const persons = res;
        console.log(persons);
        // setConversation(...conversation,persons)
      });
    //  debugger

    console.log(conversation);
    if (name.length !== 0 && email.length !== 0 && msg.length !== 0 && /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)) {
      setToggle(true);
addUserMessage(msg)
      setConversation(
        conversation.push({
          email: email,
          message: msg,
          sender: "user",
          name: name,
        })
      );
    }

    name.length === 0 ? setnameDetails(true) : setnameDetails(false);
// console.log(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email))
const res = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)
    email.length === 0 || res===false ? setemailDetails(true) : setemailDetails(false);

    msg.length === 0 ? setmsgDetails(true) : setmsgDetails(false);
  };
  return (
    <div className="App">
      {toggle ? (
        <a onClick={clickHandler}>
          {" "}
          <Widget
            type="button"
            className="widget"
            handleNewUserMessage={handleNewUserMessage}
            profileAvatar={
              "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUWFBgWFRUYGBgaGhgdHBwcGBkZHBwaGhwaHBgZGhgeJS4lIR4rHxoaJjgnKy8xNTU1HCQ7QDs0Py40NTEBDAwMEA8QHhISHj8rJSwxND00NDE0NzQ7NDQ0ND82NDQ0NDQ0NDQ0NDQ0NDQ0PTQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAAAgEDBQYHBAj/xABNEAACAQMBAwYIBw8CBQUAAAABAgADBBEhBRIxBgdBUWFxExQiMnKBkaEjM0JSsbLBFzRUYnOCkpOis8LR0uHwJFM1Q2N0oxUlg9Px/8QAGQEBAAMBAQAAAAAAAAAAAAAAAAEDBAIF/8QAJhEBAAICAgECBwEBAAAAAAAAAAECAxEhMRIEQTIzQlFhcYETIv/aAAwDAQACEQMRAD8A7NERASKnMizSSwJREQKTFX237ajWp29WsqVavxaEnLa4GvAZOgyRkggZMnt/bFO0t6ler5qDOBxJOiqO0kgeufNu0rh7yq9xcHL1DnHQq/JVeoAYHq75Ezp1Wk2nUPqSJwDYPODtC1AVit1SHAVCQ4HUKnH9Lem62XO5bMFFahXpMegBag7cEENjt3YiYktS0dw6XE00c5OzAQGuWQ9TUqw9+5j3yN3zm7LQZ8Z3iRkBadQk9x3QPaZLlukTlV3z1Woz4K2rP6RRAfYWPumJqc+D/JsVHfXJ+hBA7XE4rT58G+VYqe6uR9KGZaz56rU48Jb109Eo4HtKn3QOqRNS2ZzibNr6LdIjdVQGn+0wCn1GbRTqBgGUgg6ggggjrBEC7Iq2RmRZpJeECUREBERAREiTAlEhJAwKxEQEts0mRIqsAqycRAREx+3NpLb29Wu2op02bHWVGQveTgeuBx7nd274e6WzU/BW+GqdTVWGg/NU472bqmmyxQcvvVHO89Rmdj1sxJJ9+fXKXNyFwAN5jwH2mVW5lsxxFa7ldqVAoyxwJVL195HprlcAFjoMgbpGOggYnlp2hJ3qh3j0DoHqnupvjTGQeI6P7GOIdatb8Q9Va0SoBUrZKjOFzgsT3dGk8LIvyVVQNAMZwO86nvlypULY6ANAOoSOJE2mU1pWPZDwY6h7BK+DHUPYJKJDvULbUVPFV9gkGtEPyV9mPol+I3KJrE+zxPs1DwyO45+mZPYda5tW3re4dNclQxCkAcSuqt0A5Gg17rSnBly5qhjoAOGcaAnHQOgSYtMK7Yqz7N62BzvlWFO+pZxp4WmuD3tSPR2qejhOrbL2nRuKYq0Ki1EPBlOfURxB7DrPmO6obw00YEFT1T02m37m1r+HtwtFjjeFMEUagHQ9InA4nhgDPkgHWWVttnvimv6fUMTTuQvLqhtBN3Hg66jL0yeI+fTPylz6x09BO4zpUREQEgsnKEQIyQEASsBERAREQEREBOec9V2U2buD/nVqSeoZqZ9tMe2dDnMufKmTZUGAyFuUz60qfyx6xBDkFxW3AAoyToBK2ttu6nVjxP2Ds/zut2a7zGqw46KOwaZ/ztnsJlU8cN1Y8p3P8CYiJytIBiIFSOkf5/aUgGVI6v8AP7QKSoEAShMATERAShErEDxZe3qJXoMUdGDArxU9Y6weBB0OcdM+hOQHK5doW+8cCsmFqoOAbodendbBI6sEa4yeDMoIweBnr5HbVaxufGFJwjKtdR8q3qEAvjrVt31svbLK22x5sfjO46fTUS1RqqyqykFWAKkcCCMgj1S7O1BERAREQEREBERAREQE57zw3BNrStEQPVuqyIi5wfJIJYHvKKeGjnWdCnPLv/UcoaScVs7Znx0b9Tyfquh/NgcatnJG6QVdPJKkYIYaEEHgdP8ADLszXOAo/wDWLvChQBS4DGS1OmxJ7SSdZh+Pf9P95TaNS3YrTNYmUYlqtWCgZySdABqSeoCQS8Q6Z3T1HQ92sjUu5tWJ1t6IlAZWEkqvX/ndAEoTCUm1193V/aRgGVI6RApERCCJbaqo4lR3kSYOeEG4Vl7ZQXxqireZWJoP6FbyN780tvDtUS0BPJfuVAYecrKwPaDpJr2ryxusu581O0Haza2qn4W0qPQbXOinyPUNVHoTepznYjijt64QaJeW1Ouo/GXyf/tPrnRpcwkREBERAREQERIsYEokMd8kDArOfchD4Tae1q54CrTpA/kw6sP2UnQZz7mkbep31T599XOevRD/ABQOa8u9dr3npU/3aTCswAJPATNcuv8Ai956VP8AdpNevQSAo4syqO8mVW+JsxzrHtv/ADZ8nQ/+urrkkkUFI4AEg1PSzoO4npBm87S2DbXHx9Cm5+cVAb1OMN7567C0WjSSkowEVVHcoAH0S9M9rzNtu61iI1LR7vmws2Oab1qJ6lbeH7QJ98xVXmvqjzLzI6npa+3eP0Tp8jJ/0tHujwjfDktXm52gDpWt2HaXX3bktHm+2j863P57f0zsESf9ZT4fmXIBze7R+fbD85v6Zepc298fOuLdfRDt9KCdaiP9ZPD8y5jT5rqhPl3uB1LS+gl576HNba/8ytcP2byqD+yT75v8SP8AS33PCPdrFryD2cg0tw2el2dz7zgeqaVy05F+KKbi13jRBHhKRJbdB03lJ1K9edR1kcOtkS3WpK6sjAMrAqwPAqwwQewgxGS0TyTSJjjhwBWDAEag6yxfrmm3d9BE9FW0NCtWoHPwVRlBPErk7p9Y19cs3fmN6Jl3ub3T+OpXNbdutgXGualHwbH06SAAnvqNOsTkW2TjZ+wqnzK9mP2QT9SddlzAREpASgORIk5kl4QJREQEiJKUIgRkgIAlYCc95lx/oah+ddVj7kH2ToU59zMfeDjqua38MDnHL/Ta95/8P7qnNfeoFekx4LVQnqAB1M2PnGp42xc/jLRP/jpj7JrG0QPBtns9uRK5+Jrp8p9FGAZ4Ni1He2otUGHalTLD8YqCffPdMnS7tUCViJDoiIgIiICUU4lZGESuMc6n1CRkZKTMoiNOL8vqG7tSoR8unTb1hVX+H3zX7vzH9E/RNo5y/wDiS/8Abp9Z5q155j+iZpj2cR8Nv66RylONibIbpFe0/d1D9k7FOPcrV/8AY9kj/q2fvo1J2GXMKkgTmSYQBAASURAREQEREBERATnnMx953C/NvKw/Zpn7Z0Oc+5pcLSvh0LfV/q0/5QND50qeNr1D86hTP0L9k1ila+GrUKPRUqop9EsAeHUDn1TaedKsj7UVkdXXxdASrBgGDVMrkdOANO0THciqW/tS3HQgqOezyGA/a3ZXbvbTT5evy7SSAOgAeoACc52xzq0qdQrQoGqqkjfL7gJHzRukkdpx3Te9r2Ph6L0d9kDjdZlxvbp88DOmSuRnozNSbmtsN3Ga4Pzg6596490z08PqW38uqsD919vwNf1x/pj7r7fga/rj/TMpU5pbT5NeuO8o30KJ4q3NCpPkXbAdTUQ3vDj6JZvEqmMzZ+RHK4X61c0/BtTKab28Crg4OcDpU+6bVNT5Ecj/ABDwpNXwhqboyE3QAu92nUlvcJtkpv4+X/PS+nl489k17llykFjQWrueEZnCqu9u8QzFicHQBerpE2Ga3y15M+PUUpip4Mo++GK7wxgqRjI6wfVFNb56L71Pj20v7r7fga/rj/TH3X2/A1/XH+mXaPNCufKvCR1LRA95c/RPbT5pbX5Veue7cX6VMu3iURGZ57DnaRnArWxRTxZH3yO3dKjI7j7Z0m3rK6q6MGRgGVgcgqRkEHqxNKpc1tgBgmu3aXXPuQCbNsDZItaIoI7uiltzfwWVTrukgAHyix4DQ46JXfw+lbTz+ppnOxskBKd4gIZWVH10KHO7kdjafndgnPL4/Bt3TsXOPT3tm3HYEYd4qIZxi8bNDPSQuns4S3HzEObcb/TrHK2njZWx0/61gP8AwsPtnWJzPl0F8X2SikEeN2oGDkYVccROmS9jIiICIiAiIgIiICIiBq3LnldT2fQDEb9WoStKnnG8RjLN1KuRnvA6cj53u72uy1N6qypVqF3pqzBC7nJJXOCdBx4Ymy84+0TcbUrknyaGKSjq3fP9e+X93VNVvz5PrEnRDb9qcmaJpFqKlHVSwwSQd0ZwQT09clzUHfv3bHm27Z7y9MfaZmrn4t/Qb6pmM5mVzXuGxqKaj2vn7BMGC9rUt5Tt63rMdaXr4xrf2ddiInLhr/KflZb2QUVSzO+qooBbdGhY5IAGdNTrrjODPZsHbdG7pCrQYlc4IYYZW0JVh16jgSJyTncouL/ebO61NN09GFyGAPXvZPrHXM9zK0XC3LnO4TTUdRZQ5bHaAy59IS6aRFNs8ZLTfXs6jERKWgieHat14NFbo8JRU91SqlM+ryp7oc79ni2ttOlb0mrVm3UXicZJJ0CgdJJmG5N8tba9Zkp76uASFdVBZRxKlSQcdWczC88NB2skZQSqVgX7Mqyqx7MnH5wnP+beizbRobgPklmY9ShGDZ7DnHrEvrSJpMqbZLReIh9AREShoaxzisBs249FB7aiCc25O8nqb0lq1gXLDyRkgBRoOB165v3OnX3dmuPnvSXvwwb+Ga5yeP8ApaPoj7ZOS01x8Trcu/TY63zT5RvUNJarUp1WFGoyijXLIpJZVdW8hwhyM6DUgzufN3y4F+pp1QEuaYyyjg66DfUdGpAI6Mjr04VcH4ev+Vf6xnt2BtE215b3CnG7UUP2ox3XH6Jbu0m6vMRLy78XmPy+oYiJLkiIgIiICIiAkVbMiTmSUQPl7lAf9de/91cfvHmLvfNPeJlOUAxf3oP4VX99R8TF3vmnvE69iO3TLn4t/Qb6plOZzZwFKtcB8l2FMru43dzys72dchx0DGJW5+Lf0G+qZb5l7nNO5p/NdHH54ZT9QTy/T/BZ7fr/AI6OnRETpneLaWzKFdQtektRQcgEZwesHiPVL1napSQJTRUReCqAoGdToO3WXiuJSTz05jXaUREh017l1nxCuy8VVGHejow96zPUqgZVYcGAI7iMzFcrae9Y3Q4/AVT7FJ+yU5I19+xtmzk+BQE9qqFPvUzr6f64+r+MrWpK6sjqGVgQysAQQeIIOhE8ezNj29vveAopT3vOKrgnqBPHHZMhIyNy61HZKqZWRjSN7abzrWe/s9mLbvgnR8YzvZygXjp5+c68JrvJ771o+iPtmx861Xd2a4+c9Nf2t7+Ga5ye+9aPoj7Zxm+VH7afRfOt+mj3Px9f8q/1jPNfeb6xPTc/H1/yr/WM896MrgcSRielX4Y/Tx8nxz+5fWq8JKRXhKE5hynEgB1SQMCsREBLZOZMiUAgAJKIgfPvOvsg2+0Wq4+DuQGU9AdQA69+cN+eJpV75p7xPp/lJsGje0GoV1yDqD8pGHB0PQRk94JB0M4Jy15C3NgFy61qT1AiMoIcsQSqsnQTg8CeEnZDaLn4t/Qb6pmE5oa4W8ZAc+EoMSOplcYH6OT657RQv7r4CjZV6bON1nrI1NEB0J3iOOM9vUCZndq7FTZ97shU4blWizcN5iOJ73qkgfymLDitStvL3en6z1FMl6+M703uUBlZGVolcJ6T6hIyMlJmURGiJEzF3d/cJwtDU/J1k4dofcPszIiNkzp77ylv03T5yOv6SkfbNY5sKu9s2kPmtUX9tmx7GEu1+UV4PM2XWJ/Hq01HtGZTm+2XXt7d0roEZqzuqhlbdVgmhK6cQZZrVZ243u0abRKgwRKTiFk8qgSsRIS55zyVMWdJfnVwfUqP/MTG8nvvWj6I+2Z7lvaLcXuzbdhvI9SqWHWqhM+7emAq7KvbAmi9tVuKSlvBVaKl8qTkBlGd068DjsyMGd5Mc3xR4/c9NmrizWm06iYaRc/H1/yr/WMy/IrZDXe0KFIDKI4q1D0BEIOD3nC/nCZDZnN5tC6d6ngxbpUctmsSrBSc6IPKzg9IGeudj5HclKOz6Jp08szYNSo3nOw4dyjJwvRk8SSTsjiIh5153aZj7tkkFk5QiS5RkgIAlYCIiAiIgIiICaXzr7ONbZlYqPKpFaynpG4fKYHoIQvN0lm5oK6MjDKsrKw61YYI9hgePYO0RcW1GuP+ZTRu4soJHqOR6pp3PAu5Qtbj/Yu6TE9S+USfaqyXNXXaktxs6ofhLSqwXOm9SqEsjDvO8e5lmb5wtkNdbOuKSDL7oZQOJamwcKO07pX1wLhlZheSO1lubOlVBBbdCuB0OgAYY6MnUdhEzUwTGp09GJ3GyIiQl52vaYODUQEdG+uc92eMj4/S/wB1P01/nOb0OTdjR2lUoX9LNK4O/b1S9RArE+VSJUgcWxk8ML86b79yzZX4Mf11b+uaIwxMbiWa2aYnWnr8fpf7qfpr/OV8epf7qfpr/OeP7lmyvwY/rq39c0TlfyX2f41RsLGjiuzBqzipVcUaYGoIZiN4g517B8oROCI52RnmeNOnSUt0aQRVRRhVAUDqAGB7hLkztJES1cV1RGd2CogLMx0AAGSTA1e7G/t6wT5lKs57mWoo96idNnNeb23e7uq21KgKowNG2U/7anym9oI48S/UJ0qbqRqsQ8+9vK0yrEROnJERAREQEREBERAREQEREDm/LVTY7Qt9qID4N8ULrA+Q2Nxzp0YHrRB0zoiOCAQQQRkEagg8CDPJtjZlO5ovQqrvJUUqR1dII6iCAQeggTS+b/ar29R9k3bfDUfiGOgq0MZXdzxKjOnUCPkmBjdv7PqbKuXvKCM9lWbNxTXU0nJ+MQdC5PdqVOPJI2nZ1/Tr01q0WDowyGH0EcQR0g6ibRUphgQQCCCCCMgg8QR1Tmu2eSNxY1Gutl+UjHeq2h81h0mkOg9S8R0ZHkyrJj8uY7XY8vjxPTbYmD5OcpaF4pKEo66PSbR1I0OR0jPSPXg6TOTLMTE6lriYmNwxu29jUbqkaVdd5TqCNGVuhkboP/4cia/a2u2bMbltcUbqiNEW4Db6jXTIIOAMDVsaaATconVclq9ObY627ahVrbduBuO9taKdGelvM+Pxcs2vcVPbMpyc5OUbNWCbzu5y9RtXduOp6BknT6TrM3Em2S09orjrXmCImK29t6haUzUrvjjuqMF3I6EXp7+A6SJXEb6WTMRG5ZC5roiM7sqIoyzMQAB1kmaRTp1dtVdxA9PZ1N/Lc5VrhlPmqOrPs4nXAHo2byeutqMK1+GoWgINO2DENU6mqEYIHsPHAXienW1ulNFRFCooAVVAVQBwAA4CaseLx5ntkyZd8R0W1ulNFRFCoihVUDAVQMAAdWJfiJcoIiICIiAiIgIiICIkSYEokN2SBgViIgJqnLfkr45TV6TeCuqJ3qNUaYYa7jEa7pI9R111B2uIGm8jOV/jBa2uV8De0tKlM6b+B59PrBGuB15GRgzcpq/K3kfRvQr7xo3CfF100dSNQDggsudcZyNcEZM1+15ZXNgy0Nr0ju5wl3TUvTfT5YAyG0PAZ/F6YGW5V8hqVy3jFFjb3a6rWTTeI6KgHnaaZ4940muWnKurbVBb7Vp+Bc6JXUZo1MHGcjRejXgM6hJ0ew2hSroHo1EqIeDIwYdxI4HsjaOzaVemaVamtRDxVhkdhHUR0Eaic2rFu3dbzXpiUcMAykMpGQQQQQeBBHESc1K75E3lkWfZdfep6k2tdsr0k7jkgD1lTpqxnhtucikr+CvKL27jiQRVTvDLrjuB75mthtHXLTXNWe+G9yJM02tzg0WfwVlRrXdQ5wEUqunSSRvY7d3HbLtDkntG/OdoVfFqBP3tRI3mGulR9Rwx0t3KYritPfCbZq165U2lywL1PF9m0/Gq/Sy/FJ+Mz8D7QO3Oky3JjkGEqeNXz+NXRwQWGadPGoFNSBqDwOBjoA4naNi7Gt7Wn4O3pLTXpwNWPDLMdWPaSZkCczTWkV6ZbXm3acSGPbJAzpwrERAREQERIkwKMfbJCWwMy7AREQEjJShECMkBAErAREQERECksVqaupVlDKRgqwBBHUQdDL5EoBA0W+5tqIY1LKvVsahOc0mO4T2pkHHYGA1Ok85TlBb+a1reoOG8PB1CP2VB9ZnRIgcO5yeU20XoJQr2bWaO+HYVQ61AB5mVGAOnGTnHZOdC1T5vvM+otu7Fo3dE0a6B0JB4kFSODKw1DDXXtI4EzR/uM2PRXux2eEpadnxcmJHFR8ERUpsyOhBVlYghhwwZ9S7BuXq21CpUGHelTZhjGGZVLDHRqTNO2bzS7PpVFdjWrbpBC1XQpkajIVFyOwnB6QZ0GJFZESUoRIEcSQEASsBERAREQIkyA1k2GZUCAAlYiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgf/9k="
            }
            title="Almawiz Helpline"
            subtitle="We are away at the moment"
            titleAvatar={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAABDlBMVEX///8AAADwXCYAKkb1lB7d4+aInKkiRl/z6BGZqrVmf5AROFLM1NpEY3fu8fNVcYR3jZyquMEzVWu7xs4MDAz5+fkoKCjxazrm5uYXFxfKysrV1dX/+vi2traCgoI3NzfxYi7BwcH70sP+9uzydkipqalLS0teXl6enp4fHx/2nXxtbW3d3d2urq7+8Os8PDz849otLS2NjY35wKz0hl7yb0DxZjRVVVX3q5D3pIb6y5L4sJbzgFf82c395st5eXn6x7T1lHD84sP5vnf2nTH+8OD4s2D82rL3pUH70qD3rFH5w4L+/ez8+Lr+/e32oTj69Zj583738GP48nb796/9/Nz17D/59JH9+9P27Unfukw4AAAP2klEQVR4nO2deVvaSBzHAyJqUdSaYIBwCjm45BRUFMH7aN1a2+6+/zeyM7nIMSHJzKA8++z3jxKtCfkwv2sOJkzkPyLms2+Alv4HWTUtCYRNFURp0pWruVKmwSeTfCNbylXl7qQ+LKTSy3hH+iBsWukAinoeUmQBBaOK01nyE0ksdJQ0S/ltKYOw6YKUl0sN/e7R4viSnJcKaZYmDE0QtlCXVUPiFmGoKBwwtoxcP6aHQguEBS1RzSxuCRdOI9PsSpRgqIBAe8plfdsBqUYOsFBwf3IQVjmelPAgjJbJ5EWFtF2IQRSpyZNQaOJz9dSngohyhidqDUMcn5GHJK1CApIedjNUKHSWjCwpnwCSErtZehSaGt0hroXhgrDHcqhQG1RcVcQzMEyQQrdB0ahsJLx8/GEgnXxmKc2hKZnpFj4EBATcJWJAcRjBODQIW+guGUNFkcNWLmFBWCmzfAyoRj1c3RISpCPj5j+h3RJCncBXQzl9KBBWyuHGKqF81Qp5ClcK0yhhQFKTBiYG0+rVyuEaBIrvdpYAwhZkXAymNaqMsE5sBvb54CAifl3VOo1Ow7cHFJeVKIOwdcyOE+SonVSucE/OToK1SUAQJY/tHky7VoyOwnr6XAEdJRiI0sWvrNq1aPTwAvt0IDlImg8Eosj4HAJoj2INv0GAuGYAkiAgnRx+USKMDkGDlEk4YEbxryIDgHSwsyDg6AGOYp+oQaBKvlneH6RQJbiBq0oUNMgIL/ZalfMj8QXpNAnevt0HHNEKRlJ3yc+6/EAUArtimFPIESW3LJVkscf7gCjYPXMBtEK5ooJMaXD4xa7FIPj54+hIYFr9IuQonlIBAZ2tRSQLQdJ5bI7LI0Y4VzmiJ+d0QBimu6CsXwTC1nHrkqPZpcBcaIYVHfRogfAL6q5FICL2ANzsUQApROOI4heMLmUlT5IFIAXs3vns7BKE3il9ECbrmU68QRTsRHj7fAZCVu9kCSBMzqsU9gRJ57Hf7Ox6BsLvaXQZIJ4O7wlSx571uH04A/9e9A0Qes4OlZyEA+nkcN/p6PnpHrz0BgbIIbXwq8qjfvQAYfHH2mc3z0fgpVY0QIp44w5e4ppI40KDsBK+YV3HgIcwgmlZ0WiNIgYDjQsVg9EgBfwpnMebp1vw0h7PQegUjXPxKONCgihd7Dc5uo6dHcGBxcocZHxBoYy3SkbEYCQIvmEx9zc3oDphhKvBHGTQowySrAcDSeH3pYTr2BN0daFnASlijs55C9HLQoHksSOWMHuI3akH5xaQaKVNj0EV13X5OwKkgz8DIjzfxJ7Vg/NDC0jxnLK7I2ouNwhBCgFJPfb6CA9sphUtjqk3SdXZJG6QY/zBUeH+JvZwrx5dWaIWIKFapUDxzmUSLhAFf/IAJsPY0yU8Ei5sINEp7SZhcikfEJFgqvPyNRa7u1UPW2MbSPGcduBipMUgafxcCJJIzARhpjaQaJ9oGBslx1CEE2SI7yHM0ZkFZHRibxLKBRfwEmkRCEvSIJd3FpCy3baigysad2+VnF4AIpLMot8/AJCnmfZDq2YHARUXldufqyEtACGYCGGER8Chh1+gXtFBUqMduareIApJgxw9Q5CbM/1Hp22BDhblyJXteIEQ9KcYLYsAXes/tkYOkOgh5bRoG6+zgZANvc/uVJAn43MvHzpJ6I6nMFxG8QA5Jlr3c/+kgjxc6iStU6eX0JkpmYsT0SDpCclVX54fVJCbxyP9N44yRSOhcP9zdRUkSKGEf0nh5de1BhJ7ujV+d+oyLsoklmreCiIReMivb9+vXzWQ2L1hP+2+CyQ6vqJBoIuro0BY/EFS5uXbO2OCGMkd5BJ3kwCPp+gn8+xuASlgDy4yzPtfvxjdR4AejVtt1Vz+DqMwPZKSiACRCHpUP/4wzOOTAXJ3afxH2e3vgOSKBoMqvo4AwZ5nY5hvb3+B8HtngNxcG5+54CpUoCjW9F03CEswof729s3M7FoINv4HaVxRsrUpVuXSLhD8CSrmZfPtl94d0fW6MHJFD6l1GLOiEwR/5hNY1ubP7wwcZbSQzIxbvUK5CbUUz+dZJwhBBf9HA5k9zUHMfgkjnJ+4QegNEOWcIGmCCv5t8+cLY7ctQGJ0TFqnbpAoyZo6m7KKA6RA0Fn/vfn2N3yd3VhJHoyiqz1FgNCakONFOwg7JOiKbG7+foevt3dWENDHutU8od1HhK4+HeNK1lkbSBp/5BqCbP6Br0ePtiYxM6Nw4ewtqjGYDoheARsgJMsWIQiMv+rYL5oEFbrodLM4fcjRAOmQfJUQgGz+UG/47MZJogUvoecOXXQiF5ct2EBIfB04O9A79AeHl6htovqJgMqLVCJXUlt1boCQDPkyPzdVEphLnF5iWtcFoqIfUEmLQ8UKIpFc6l0F+Q0KRzgZ6tS1GoWFMiJyURnqqhesIHWSS/36rZH8AG1y/+oE0Ue6BEQ2OaGRTPKiFYSgdwj0Y1PTjxdHetcyo+bw7YGbhMa8SVeyghBM7wD9etNJ/nxH+LsxHOEeHqKyTqU6sYKQrFIGev+tk7y/CJfOZAKMS3UTVKkyJg9cua4VhKC/DvXdMK63P4hkEnvQI5c7LVLomWRkKwjBkJaqvw3j+o2MXI+qcbmHg2nMwWebVhDiL0J/M4yLQaXFa62mv0D4O/GYCl+ygpAkdp3kHxOEuXxygDypywiY1tTt78QjEcmMFYTCdgeqdf3zUz2eORz+QV3Y4VhHoKlIOmDHZa0glL9/K9zbSR70WZM2op4fkTZJwwpCdtvlWmVQGZ9aS6d7m3Xd6eNDqNqxT5pLeFogQrk/KEaLxcPK1LJ6ZnY3j8Kv98ZwsHOKFEbgPsmbMxRBynPDL55fCEaz3F6/6iivZ8aUCSq7Rw+JMIC30wKx2n2xUjO+XSwczZ4fAMrr9czkQIKcEGFQBHHcVqU/KnsWHqgBFVIQaqblurPiuHZebqOiKipq0QUhmc5F3BsoPWq9C9d38VsjRD+RGMQWfkkSIhIEaNAfXdlYWqjRU2IQe0IkKVEQBZRuYieDSr923itftNsX5d6oj2oP8GdkIPYShaRoROQG620eVsbjfn88rqAxQMuRgdiLRpJFKO4lDqFUIczs9jKepGOFnpUKLNKulb1jRdTVbaPmcoJqStrZLdm6ukSDDwJqPj2YiuQTo03b4APZcJB94XUI9SmsHpBtw0FEA3Remc5HJ5UajXlq+wAd0ZCpSoLMdcB2gNC/H+Ds+YKQfchUJN33q9VDeTyouk5r04oL8mRc64XdTMhLkm0Q+5i40y6UUeNv5XZLzernIwA0nfb702ntdARzPa2Z9qRom1YgmujR1R6NnVY0X54lAKD2BVCbVktock70EE29mQJ9dwdKn9on7yHn1BvRZKhF5aljKTnlxX8uJWX7ZCjZStm5hFZ5ZB8pOaG3ggalpL4J13zBAB0QoNZFrza2tMuASrbwEi/a59mJlnC41CqfT0HZbvhLv7e8Rmno376gsqgGqXbvdFo5PFHzYfF0aT7vWlQTIVjmhJYgCG3QLZyOB0U6RRVK7mVOkeMP2reQrrLDiBOEZCng5ymnuEAiBJuBfZ7kiBuEYLnsp4mfIECOCSdEP0MZ1ALmUF8UW19bW9tYwp1tb6zthCiWkEvKQy3yXxbIBrjul+B/jlzkHyoALwkEXjbEdRsiEiTMni7LapEEuO5u4L+2fDsU96tJK+Ej3DCCBlGCdxOXBRJGXCblAcIG32dnFUDMOssFEkl5uvt+HLrh7oHxsxUERJotZn8H/GIf/HSwC/9w3/Pcg7kX7K2tJdbVo23wyz148GUNXkzVmkVx1D1lrVtYBPuK6/ZX44p7aJDdPeMN95xv7ToX+nPSPIobSGsHYUGarDfIEDlPsm255BYSxFAibh4eeJ0L73Xf/C+tcQBswvxPBIi1gQ01bLujOL8Gjh7MBrf6ZVv73BIeIODj1iC+HmiNsON17r7ROGrjJWDjJM00aAHRBf/8K+qmqsoCEI9Natbj6+bbbCNB9o3/Vo1ezWte5yYT+p3BZtDO3DI/dBfIesJ0JLsc29U4QRZvGxQ3zdUJor7VltVU1ta9zt3VnARcIrGrNc6u6TYukK8ehsVUOwtBIsNFqWTLH0SzkB03yPzcuHZv4GV3S7tKwgxkTpA9S4Cxy2fzCs+Ng+LxhDWAhAJxnJvUGg40w1ZStbfteVxygHg6CFPq+ICwIsJLkkZQxQBxn6vFqKRqTjvwl3HTshwgyQ0PB2F4546NqC13XMa1Dm91N76PY1qIc9Ub395XP+s4DG+784/dDrK75pFCuJxz/zbEJkju7/GB20psG3caEgRxrmZKe+qP8BCGMeN2bSDxNa9SOOt6bgxqWyrncznmUT48COpc9Yy9hBaNgZnFzaBuB4GQG8hSmJODbEvl2trQrI6SO6FBUOdqRZZuTtqhWX9aQKCDmIB2Zdy70CG3bqvbPwb4qcKrxzfCOzvqXA1PD6uWQweIp4MgN51EgqQchcqOEXU2sHzEeS6jlYt6NaYemilvDnKwZpFZdatqIvbHRm9v6Nhjyyj8dpJfQ4MgztU/7YTr0Aay5QnCo54Q5bHhpMO4tuH1d7bUOBI2j7jPZbRE92V+OA9NAUC4fPANJyPp6sqOn3I55HPUvDZlPSb9+sLSlBGRN7yEbXKXKy6Pvl/vjYtJdtpaouSwGxdHUis5pu35ZIIFm3sfU39KILkaaAdZDMJKK0fSqONstx5hJyvm8ElkBvEHWTmHlxc8iXMhCCi6VikvNhc9PmkxCOjBrwyJz4N6fEAiqZXJ8JnFj4TxAyHaPYymMj5PffMFWZHJ3pJnAgkMshL1o0elGA4EWNcnezznzxEIhGTbcioqFfyfwBcIxNWJ/1ihuuiYIJFO99OqFV4O9LjggCCg7vqkCrKRD/aE8KAgkYiE/4hKEg7E0wcIQdjPSCilwM8GDw4CHeUDHtZsFRfMPUKDRNKTD80omYDuER4EZPnmh0UvPuefBfFBQKN80IJBPkxzYIBE0scf0tlqiuEeBh4eBPh8nWjH6SAqTYJ7OT4I8JTuMnMKl5VDeQcBSCQiVnGf0+6LweeG/u9PDYRlh0uyr5KUDpoDaYAAlI4kUw/FyarUwcIgAAFKSTLVSrJRrQd/sDxNEBCLpWqWjrNwfLZaDxlxKYIAFSYlChUYl8kH6AUuFSTCpsQuoYVl5WGKpDXogAApx3W5hOn5fEaui0EewO4jKiBAabHezYX1Fz6bAxThaiov0QKBUsR8M8vzSc4Xh+OSSUCRH9KBgKIJAvOkoraMj/sns7kusCeWzL3togsSUVlSBXFYz3eruUwWNI/ZCqANMqWqnK9LYiGl4OVvb1EHUcUqBVGadOVmKZNtAGNL8nwDUDTl7gRQKJQZVC0H5BP0P8iq6X+QVdN/BuRfi6jBGkMDG7AAAAAASUVORK5CYII="}
          ></Widget>{" "}
        </a>
      ) : (
        ""
      )}

      {conversation.length === 0 && flag ? (
        <div className="container">
          <input
            ref={nameRef}
            placeholder="Enter your full name"
            className="inputs"
            type="text"
          ></input>
          {nameDetails ? (
            <span className="warningmsg">Enter valid user name</span>
          ) : (
            ""
          )}

          <input
            placeholder="Enter your email"
            ref={emailRef}
            className="inputs"
            type="email"
          ></input>
          {emailDetails ? (
            <span className="warningmsg">Enter valid eamilID</span>
          ) : (
            ""
          )}

          <textarea
            className="inputs testarea"
            ref={messageRef}
            placeholder="Type message"
          />
          {msgDetails ? <span className="warningmsg">Enter Message</span> : ""}

          <input
            className="inputs"
            onClick={getUserDetails}
            type="submit"
            value="Start  conversation"
          ></input>
        </div>
      ) : (
        ""
      )}
    </div>
  );
}

export default WidgetApp;
