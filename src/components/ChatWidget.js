import React, { useEffect, useState } from "react";
import { Widget, addResponseMessage, addUserMessage } from "react-chat-widget";
import "react-chat-widget/lib/styles.css";
import "./widget.css";
import axios from "axios";
import almawiz from "./assets/almawiz_logo.png";
import avatar from "./assets/avatar.jpg";
// import Label from "./Label";
// import {messageHandler} from '../api'

function ChatWidget() {
  console.log("Entry Point");
  const [start, setStart] = useState(true);
  const [conversation_id, setId] = useState(237);
  const [status, setStatus] = useState("online");
  const [conversation, setConversation] = useState([]);
  const [agent_id, setAgentid] = useState(6414);
  const [data, setData] = useState([]);
  const [messageid, setMessageId] = useState();
  const [datamid, setDatamid] = useState();
  const [count, setCount] = useState();

  // const [bigin, setBigin] = useState(true);
  console.log("Inside widget");
  useEffect(() => {
    console.log("Inside useEffect");
    console.log();
    setMessageId(
      conversation.length !== 0
        ? conversation[conversation.length - 1].messageid
        : ""
    );
    setDatamid(data.length !== 0 ? data[data.length - 1].messageid : "");
    // console.log(data.length!==conversation.length)
    console.log(datamid !== messageid);

    for (var j = 0; j < conversation.length; j++) {
      if (messageid === datamid) {
        setCount(j);
      }
    }

    if (datamid !== messageid) {

      for (var i = count; i < conversation.length; i++) {
        console.log(conversation[i].message);
        
console.log(conversation[i].message)
        if (
          conversation[i].sender !== null
            ? conversation[i].sender.type !== "contact"
            : false
        ) {
          // addUserMessage(conversation[i].message)
          addResponseMessage(conversation[i].message);
        }
       
      }
    }
    setData(conversation);

  }, [conversation]);

  const handleNewUserMessage = async (newMessage) => {
    var conData = "";
    console.log(conversation.length);
    setInterval(async () => {
      var mid = "";
      await axios
        .post(
          "https://chat-server-pritam.herokuapp.com/account/conversations/messages",
          {
            agentid: 6414,
            conversationid: 237,
          }
        )
        .then((data1) => {
          // console.log(data.data.data);
          conData = data1.data.data;

          console.log(conData);
        });
      console.log("Inside SetInterval");
      setConversation(conData);
    }, 10000);
    // conversation.forEach((info)=>{
    // addResponseMessage(info.message)
    // })
    // debugger

    // setMessageId(conData[conData.length-1].messageid)

    setData(conversation);
    const message = {
      message: newMessage,
      conversationid: conversation_id,
      type: "incoming",
    };
    // var value = "";
    if (start) {
      axios
        .post("https://chat-server-pritam.herokuapp.com/createConversation", {
          message: newMessage,
        })
        .then((res) => {
          const result = res;

          setId(237);
          setAgentid(6414);
          console.log(agent_id);
          console.log("Inside start");
          setStatus(result.data.save.agentDetails.availability_status);

          console.log(res);
        });
      console.log("first");
      // setStart(false);
    }
    if (true) {
      axios
        .post("https://chat-server-pritam.herokuapp.com/sendMessage", message)
        .then((res) => {
          console.log(res);
          console.log("inside send");
        });
    }
    // setConversation(list);
  };

  return (
    <div className="App">
      <Widget
        type="button"
        className="widget"
        handleNewUserMessage={handleNewUserMessage}
        profileAvatar={avatar}
        showTimeStamp={false}
        title="Almawiz Helpline"
        subtitle={status}
        titleAvatar={almawiz}
        launcherOpenLabel="hello"
      ></Widget>
    </div>
  );
}

export default ChatWidget;
